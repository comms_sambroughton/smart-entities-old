<?php

namespace CommsExpress\SmartEntities;

use CommsExpress\SmartEntities\Commands\CreateAuthProvider;
use CommsExpress\SmartEntities\Commands\CreateModel;
use CommsExpress\SmartEntities\Commands\CreateProvider;
use CommsExpress\SmartEntities\Commands\CreateRepository;
use CommsExpress\SmartEntities\Commands\CreateRepositoryContract;
use CommsExpress\SmartEntities\Commands\CreateSmartEntity;
use CommsExpress\SmartEntities\Commands\CreateSmartUserEntity;
use CommsExpress\SmartEntities\Commands\CreateUserModel;
use CommsExpress\SmartEntities\Commands\CreateUserService;
use CommsExpress\SmartEntities\Commands\GenerateSmartEntityPattern;
use CommsExpress\SmartEntities\Commands\MakeSmartAuthCommand;
use Illuminate\Support\ServiceProvider;

class SmartEntitiesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateSmartEntityPattern::class,
                CreateSmartEntity::class,
                CreateRepositoryContract::class,
                CreateRepository::class,
                CreateProvider::class,
                CreateModel::class,
                MakeSmartAuthCommand::class,
                CreateUserModel::class,
                CreateSmartUserEntity::class,
                CreateAuthProvider::class,
                CreateUserService::class,
            ]);
        }
    }

    public function register()
    {

    }
}