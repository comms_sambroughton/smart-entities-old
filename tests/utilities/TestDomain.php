<?php

namespace Tests\Utilities;

use App\Repositories\Base\BaseRepositoryContract;
use App\Repositories\Thread\ThreadRepositoryContract;

class FooEntity extends \App\Entities\BaseEntity
{
    public function __construct(BaseRepositoryContract $repo)
    {
        parent::__construct($repo);
    }
}
