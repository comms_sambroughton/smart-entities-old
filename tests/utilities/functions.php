<?php

use App\Classes\EntityHelpers;

function create($entityClass, $attributes = [], $times = 1)
{
    $modelClass = array_search($entityClass, EntityHelpers::$modelMap);
    $models = factory($modelClass, $times)->create($attributes);
    return $times == 1
        ? app()->make($entityClass)->buildEntity($models->first()->toArray())
        : app()->make($entityClass)->buildEntities($models->toArray());
}

function raw($entityClass, $attributes = [], $times = 1)
{
    $modelClass = array_search($entityClass, EntityHelpers::$modelMap);

    return $times == 1
        ? factory($modelClass, $times)->raw($attributes)[0]
        : factory($modelClass, $times)->raw($attributes);
}