<?php

namespace CommsExpress\SmartEntities\Tests;

use App\Entities\UserEntity;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function signIn($user = null)
    {
        $user = $user ?: create(UserEntity::class);
        $this->be($user);
        return $user;
    }

    public function makeTestTables(array $tables)
    {
        foreach ($tables as $name => $data) {
            $this->app['db']->connection()->getSchemaBuilder()->create($name, function (Blueprint $table) use ($data) {
                $table->increments('id');
                foreach ($data as $name => $type) {
                    $table->{$type}($name);
                }
                $table->timestamps();
            });
        }
    }

    protected function getMockFor($class, $methods = [], $useOriginalConstructor = false, $arguments = [])
    {
        $builder = $this->getMockBuilder($class);

        $builder = $useOriginalConstructor
            ? $builder->setConstructorArgs($arguments)
            : $builder->disableOriginalConstructor();

        $mock = $builder->setMethods(array_keys($methods))
            ->getMock();

        foreach ($methods as $name => $returns) {
            $mock->method($name)->willReturn($returns);
        }
        return $mock;
    }

}
