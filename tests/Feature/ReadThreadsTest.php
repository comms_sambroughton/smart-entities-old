<?php

namespace Tests\Feature;

use App\Entities\ChannelEntity;
use App\Entities\ThreadEntity;
use App\Entities\UserEntity;
use App\Models\Thread\MySQL\Thread;
use App\Entities\ReplyEntity;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReadThreadsTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;
    protected $threadClass;

    public function setUp()
    {
        parent::setUp();

        $this->threadClass = app()->make(ThreadEntity::class);
        $this->thread = create(ThreadEntity::class);
    }

    /** @test */
    public function a_user_can_view_all_threads()
    {
        $this->withoutExceptionHandling();
        $this->get('/threads')
            ->assertSee($this->thread->title)
            ->assertSee($this->thread->body);
    }

    /** @test */
    public function a_user_can_view_a_single_thread()
    {
        $this->withoutExceptionHandling();
        $this->get($this->thread->path())
            ->assertSee($this->thread->title);
    }

    /** @test */
    public function a_user_can_read_replies_that_are_associated_with_a_thread()
    {
        $this->withoutExceptionHandling();
        $reply = create(ReplyEntity::class, ['thread_id' => $this->thread->getId()]);
        $this->get($this->thread->path())
            ->assertSee($reply->body);
    }

    /** @test */
    public function a_user_can_filter_threads_by_a_channel()
    {
        $this->withoutExceptionHandling();
        $channel = create(ChannelEntity::class);
        $threadInChannel = create(ThreadEntity::class, ['channel_id' => $channel->id]);
        $threadNotInChannel = create(ThreadEntity::class);

        $this->get('/threads/' . $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);
    }

    /** @test */
    public function a_user_can_filter_threads_by_username()
    {
        $this->withoutExceptionHandling();
        $this->signIn($user = create(UserEntity::class, ['name' => 'JaneDoe']));

        $threadByJane = create(ThreadEntity::class, ['user_id' => auth()->id()]);
        $threadNotByJane = create(ThreadEntity::class);

        $this->get('/threads?by=JaneDoe')
            ->assertSee($threadByJane->title)
            ->assertDontSee($threadNotByJane->title);
    }
}
