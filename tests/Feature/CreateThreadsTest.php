<?php

namespace Tests\Feature;

use App\Entities\ChannelEntity;
use App\Entities\ThreadEntity;
use App\Entities\UserEntity;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateThreadsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function an_authenticated_user_can_create_new_forum_threads()
    {
        $this->withoutExceptionHandling();
        $this->be($user = create(UserEntity::class));

        $response = $this->post('/threads', $thread = raw(ThreadEntity::class));

        $this->get($response->headers->get('Location'))
            ->assertSee($thread['title'])
            ->assertSee($thread['body']);
    }

    /** @test */
    public function guests_may_not_create_threads()
    {
        $this->get('/threads/create')
            ->assertRedirect('login');
        $this->post('/threads')
            ->assertRedirect('login');
    }

    /** @test */
    public function a_thread_requires_a_title()
    {
        $this->publishThread(['title' => null])
            ->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_thread_requires_a_valid_channel()
    {
        create(ChannelEntity::class, [], 2);

        $this->publishThread(['channel_id' => null])
            ->assertSessionHasErrors('channel_id');

        $this->publishThread(['channel_id' => 999])
            ->assertSessionHasErrors('channel_id');
    }

    private function publishThread($overrides)
    {
        $this->signIn();

        $thread = raw(ThreadEntity::class, $overrides);

        return $this->post('/threads', $thread);
    }
}
