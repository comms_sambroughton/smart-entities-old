<?php

namespace Tests\Feature;

use App\Repositories\Base\BaseRepositoryContract;
use App\Repositories\Base\MySQL\BaseRepository;
use App\Models\Base\MySQL\Base as BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;
use Tests\TestCase;

class RepositoryTest extends TestCase
{
    /** @test */
    public function it_can_perform_an_all_query()
    {
        $model = new RepositoryTestModelStub;
        $repo = new RepositoryTestRepoStub($model);

        $repo->all();

        $this->assertEquals(['all-query-result'], $repo->getArrayFromQueryResult());
    }

    /** @test */
    public function it_can_perform_a_first_query()
    {
        $queryStub = new RepositoryTestQueryBuilderStub;
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['newQuery' => $queryStub]);

        $repoStub->first();
        $this->assertEquals(['first-query-result'], $repoStub->getArrayFromQueryResult());
    }

    /** @test */
    public function it_can_perform_a_where_query()
    {
        $queryStub = new RepositoryTestQueryBuilderStub;
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['newQuery' => $queryStub]);

        $repoStub->where('name', '=', 'foo');
        $this->assertEquals(['where-name=foo-query-result'], $repoStub->getArrayFromQueryResult());
    }

    /** @test */
    public function it_can_perform_a_with_query()
    {
        $queryStub = new RepositoryTestQueryBuilderStub;
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['newQuery' => $queryStub]);

        $repoStub->with('foos');
        $this->assertEquals(['with-foos-query-result'], $repoStub->getArrayFromQueryResult());
    }

    /** @test */
    public function it_can_perform_a_get_query()
    {
        $queryStub = new RepositoryTestQueryBuilderStub;
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['newQuery' => $queryStub]);

        $repoStub->get();
        $this->assertEquals(['get-query-result'], $repoStub->getArrayFromQueryResult());
    }

    /** @test */
    public function it_can_perform_a_chained_query_ending_in_first()
    {
        $queryStub = new RepositoryTestQueryBuilderStub;
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['newQuery' => $queryStub]);

        $repoStub->with('bars')->where('name', '=', 'Foo')->first();

        $this->assertEquals([
            'with-bars-query-result',
            'where-name=Foo-query-result',
            'first-query-result',
        ], $repoStub->getArrayFromQueryResult());
    }

    /** @test */
    public function it_can_perform_a_chained_query_ending_in_get()
    {
        $queryStub = new RepositoryTestQueryBuilderStub;
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['newQuery' => $queryStub]);

        $repoStub->with('bars')->where('name', '=', 'Foo')->get();

        $this->assertEquals([
            'with-bars-query-result',
            'where-name=Foo-query-result',
            'get-query-result',
        ], $repoStub->getArrayFromQueryResult());
    }

    /** @test */
    public function it_can_create_a_model()
    {
        $modelStub = $this->getMockFor(RepositoryTestModelStub::class, ['create' => collect('create-called')]);
        $repoStub = new RepositoryTestRepoStub($modelStub);

        $modelStub->expects($this->once())->method('create')->with(['name' => 'Foo']);
        $this->assertEquals(['create-called'], $repoStub->create(['name' => 'Foo']));
    }

    /** @test */
    public function it_can_update_a_model()
    {
        $queryStub = $this->createMock(RepositoryTestQueryBuilderStub::class);
        $queryStub->method('where')->willReturn($queryStub);
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['newQuery' => $queryStub]);

        $queryStub->expects($this->once())->method('update')->with(['name' => 'updated']);
        $repoStub->update(['name' => 'updated'], 1, 'id');
    }

    /** @test */
    public function it_can_update_using_the_query_builder()
    {
        $queryStub = $this->createMock(RepositoryTestQueryBuilderStub::class);
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['getQuery' => $queryStub]);

        $queryStub->expects($this->once())->method('update')->with(['name' => 'updated']);
        $repoStub->update(['name' => 'updated']);
    }

    /** @test */
    public function it_can_delete_a_model()
    {
        $queryStub = $this->createMock(RepositoryTestQueryBuilderStub::class);
        $queryStub->method('where')->willReturn($queryStub);
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['newQuery' => $queryStub]);

        $queryStub->expects($this->once())->method('delete');
        $repoStub->delete(1, 'id');
    }

    /** @test */
    public function it_can_delete_using_the_query_builder()
    {
        $queryStub = $this->createMock(RepositoryTestQueryBuilderStub::class);
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['getQuery' => $queryStub]);

        $queryStub->expects($this->once())->method('delete');
        $repoStub->delete();
    }

    /** @test */
    public function it_can_attach_a_relation()
    {
        $relationStub = $this->createMock(RepositoryTestRelationStub::class);
        $queryStub = $this->getMockFor(RepositoryTestQueryBuilderStub::class, ['bars' => $relationStub]);
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['getQuery' => $queryStub]);

        $relationStub->expects($this->once())
            ->method('attach')
            ->with(222, ['pivotAttribute' => 'FooBar']);
        $repoStub->attachRelation(111, 'id', 'bars', 222, ['pivotAttribute' => 'FooBar']);
    }

    /** @test */
    public function it_can_attach_multiple_relations()
    {
        $relationStub = $this->createMock(RepositoryTestRelationStub::class);
        $queryStub = $this->getMockFor(RepositoryTestQueryBuilderStub::class, ['bars' => $relationStub]);
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['getQuery' => $queryStub]);

        $relationStub->expects($this->once())
            ->method('attach')
            ->with([222 => ['pivotAttribute' => 'FooBar']]);
        $repoStub->attachRelations(111, 'id', 'bars', [222 => ['pivotAttribute' => 'FooBar']]);
    }

    /** @test */
    public function it_can_detach_a_relation()
    {
        $relationStub = $this->createMock(RepositoryTestRelationStub::class);
        $queryStub = $this->getMockFor(RepositoryTestQueryBuilderStub::class, ['bars' => $relationStub]);
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['getQuery' => $queryStub]);

        $relationStub->expects($this->once())
            ->method('detach')
            ->with(222);
        $repoStub->detachRelation(111, 'id', 'bars', 222);
    }

    /** @test */
    public function it_can_detach_multiple_relations()
    {
        $relationStub = $this->createMock(RepositoryTestRelationStub::class);
        $queryStub = $this->getMockFor(RepositoryTestQueryBuilderStub::class, ['bars' => $relationStub]);
        $repoStub = $this->getMockFor(RepositoryTestRepoStub::class, ['getQuery' => $queryStub]);

        $relationStub->expects($this->once())
            ->method('detach')
            ->with([222, 333]);
        $repoStub->detachRelations(111, 'id', 'bars', [222, 333]);
    }
}

class RepositoryTestQueryBuilderStub
{
    protected $query = [];

    public function get()
    {
        array_push($this->query, 'get-query-result');
        return $this;
    }

    public function where($column, $operator, $value)
    {
        array_push($this->query, "where-{$column}{$operator}{$value}-query-result");
        return $this;
    }

    public function first()
    {
        array_push($this->query, 'first-query-result');
        return $this;
    }

    public function with($relation)
    {
        array_push($this->query, "with-{$relation}-query-result");
        return $this;
    }

    public function toArray()
    {
        return $this->query;
    }

    public function create($attributes)
    {
        return collect(array_merge(['model' => 'created'], $attributes));
    }

    public function update($attributes)
    {
        return array_merge(['model' => 'updated using query builder'], $attributes);
    }

    public function delete()
    {
        return array_merge(['model' => 'deleted using query builder']);
    }
}


class RepositoryTestModelStub extends BaseModel
{
    public static function query()
    {
        return (new static([], new RepositoryTestQueryBuilderStub))->newQuery();
    }

    public static function get($columns = [])
    {
        return collect('all-query-result');
    }

}

class RepositoryTestRepoStub extends BaseRepository
{
}

class RepositoryTestRelationStub extends BelongsToMany {}
