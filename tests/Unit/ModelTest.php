<?php

namespace Tests\Feature;

use App\Repositories\Base\BaseRepositoryContract;
use App\Repositories\Base\MySQL\BaseRepository;
use App\Models\Base\MySQL\Base as BaseModel;
use Tests\TestCase;

class ModelTest extends TestCase
{
    /** @test */
    public function can_arrayify_a_singular_relation()
    {
        $foo = new EloquentModelStub(['name' => 'Foo']);
        $bar = new EloquentModelStub(['name' => 'Bar']);

        $foo->setRelation('bar', $bar);

        $barArray = $this->getExpectedArray(get_class($bar), ['name' => 'Bar']);
        $fooArray = $this->getExpectedArray(get_class($foo), ['name' => 'Foo'], ['bar' => $barArray]);
        $this->assertEquals($fooArray, $foo->toArray());
    }

    /** @test */
    public function it_can_arrayify_a_plural_relation()
    {
        $foo = new EloquentModelStub(['name' => 'Foo']);
        $barA = new EloquentModelStub(['name' => 'BarA']);
        $barB = new EloquentModelStub(['name' => 'BarB']);

        $foo->setRelation('bars', collect([$barA, $barB]));

        $barArrayA = $this->getExpectedArray(get_class($barA), ['name' => 'BarA']);
        $barArrayB = $this->getExpectedArray(get_class($barB), ['name' => 'BarB']);
        $fooArray = $this->getExpectedArray(get_class($foo), ['name' => 'Foo'], ['bars' => [$barArrayA, $barArrayB]]);
        $this->assertEquals($fooArray, $foo->toArray());
    }

    /** @test */
    public function it_can_arrayify_a_relation_with_pivot_data()
    {
        $foo = new EloquentModelStub(['name' => 'Foo']);
        $bar = new EloquentModelStub(['name' => 'Bar']);

        $bar->setRelation('pivot', $bar->newPivot($foo, ['name' => 'FooBar'], 'some-table', true));
        $foo->setRelation('bars', collect($bar));

        $barArray = $this->getExpectedArray(get_class($bar), ['name' => 'Bar'], [], ['name' => 'FooBar']);
        $fooArray = $this->getExpectedArray(get_class($foo), ['name' => 'Foo'], ['bars' => $barArray]);
        $this->assertEquals($fooArray, $foo->toArray());
    }

    /** @test */
    public function it_can_arrayify_a_relation_with_its_own_relation()
    {
        $foo = new EloquentModelStub(['name' => 'Foo']);
        $bar = new EloquentModelStub(['name' => 'Bar']);
        $baz = new EloquentModelStub(['name' => 'Baz']);

        $bar->setRelation('bazs', collect($baz));
        $foo->setRelation('bars', collect($bar));

        $bazArray = $this->getExpectedArray(get_class($bar), ['name' => 'Baz']);
        $barArray = $this->getExpectedArray(get_class($bar), ['name' => 'Bar'], ['bazs' => $bazArray]);
        $fooArray = $this->getExpectedArray(get_class($foo), ['name' => 'Foo'], ['bars' => $barArray]);
        $this->assertEquals($fooArray, $foo->toArray());
    }

    protected function getExpectedArray($model = EloquentModelStub::class, $attributes = [], $relations = [], $pivot = [])
    {
        return [
            "model" => $model,
            "attributes" => $attributes,
            "relations" => $relations,
            "pivot" => $pivot
        ];
    }
}

class EloquentModelStub extends BaseModel {}