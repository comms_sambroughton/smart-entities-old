<?php

namespace Tests\Feature;

use App\Classes\EntityHelpers;
use App\Classes\QueryBuilder;
use App\Entities\BaseEntity;
use App\Models\Base\MySQL\Base;
use App\Repositories\Base\BaseRepositoryContract;
use App\Repositories\Base\MySQL\BaseRepository;
use App\Repositories\Thread\MySQL\ThreadRepository;
use App\Repositories\Thread\ThreadRepositoryContract;
use Illuminate\Support\Collection;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Utilities\FooEntity;

class QueryBuilderTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    protected $fooRepo;
    protected $fooEntity;

    /** @test */
    public function it_can_retrieve_first_entity()
    {
        list($fooRepo, $fooEntity) = $this->mockEntityWithRepo('Foo');

        $fooRepo->method('getArrayFromQueryResult')->willReturn($this->getModelArray('FooModel', ['name' => 'Foo']));
        $fooRepo->expects($this->once())->method('first');

        $foo = $fooEntity->first();
        $this->assertInstanceOf('FooEntity', $foo);
        $this->assertEquals('Foo', $foo->name);
    }

    /** @test */
    public function it_can_retrieve_all_entities()
    {
        list($fooRepo, $fooEntity) = $this->mockEntityWithRepo('Foo');

        $fooRepo->method('getArrayFromQueryResult')->willReturn([
            $this->getModelArray('FooModel', ['name' => 'FooA']),
            $this->getModelArray('FooModel', ['name' => 'FooB']),
        ]);
        $fooRepo->expects($this->once())->method('all');

        $foos = $fooEntity->all();
        $this->assertInstanceOf(Collection::class, $foos);
        $this->assertInstanceOf('FooEntity', $foos->first());
        $this->assertEquals('FooA', $foos->first()->name);
        $this->assertEquals('FooB', $foos->last()->name);
    }

    /** @test */
    public function it_can_retrieve_an_entity_with_singular_relation()
    {
        list($fooRepo, $fooEntity) = $this->mockEntityWithRepo('Foo');
        $this->mockEntityWithRepo('Bar');

        $fooRepo->method('getArrayFromQueryResult')->willReturn(
            $this->getModelArray('FooModel', [],
                ['bar' => $this->getModelArray('BarModel', ['name' => 'Bar'])]
            ));

        $fooRepo->expects($this->once())->method('with')->with('bar');
        $fooRepo->expects($this->once())->method('first');

        $foo = $fooEntity->with('bar')->first();
        $this->assertInstanceOf('BarEntity', $foo->bar);
        $this->assertEquals('Bar', $foo->bar->name);
        $this->assertInstanceOf('BarEntity', $foo->getRelation('bar'));
        $this->assertEquals('Bar', $foo->getRelation('bar')->name);
    }

    /** @test */
    public function it_can_retrieve_an_entity_with_plural_relation()
    {
        list($fooRepo, $fooEntity) = $this->mockEntityWithRepo('Foo');
        $this->mockEntityWithRepo('Bar');

        $fooRepo->method('getArrayFromQueryResult')->willReturn(
            $this->getModelArray('FooModel', [],
                ['bars' => [
                    $this->getModelArray('BarModel', ['name' => 'BarA']),
                    $this->getModelArray('BarModel', ['name' => 'BarB']),
                ]]
            ));

        $fooRepo->expects($this->once())->method('with')->with('bars');
        $fooRepo->expects($this->once())->method('first');

        $foo = $fooEntity->with('bars')->first();
        $this->assertInstanceOf(Collection::class, $foo->bars);
        $this->assertInstanceOf('BarEntity', $foo->bars->first());
        $this->assertEquals('BarA', $foo->bars->first()->name);
        $this->assertEquals('BarB', $foo->bars->last()->name);
    }

    /** @test */
    public function it_can_query_an_entity_by_attribute()
    {
        list($fooRepo, $fooEntity) = $this->mockEntityWithRepo('Foo');

        $fooRepo->method('getArrayFromQueryResult')->willReturn([
            $this->getModelArray('FooModel', ['name' => 'Foo'], []),
        ]);
        $fooRepo->expects($this->once())->method('where')->with();
        $fooRepo->expects($this->once())->method('get');

        $result = $fooEntity->where('name', '=', 'Foo')->get();
        $this->assertEquals(1, $result->count());
        $this->assertEquals('Foo', $result->first()->name);
    }

    /** @test */
    public function it_can_query_an_entity_using_multiple_conditions()
    {
        list($fooRepo, $fooEntity) = $this->mockEntityWithRepo('Foo');

        $fooRepo->method('getArrayFromQueryResult')->willReturn([
            $this->getModelArray('FooModel', ['name' => 'Foo', 'bar_id' => 1], []),
        ]);

        $fooRepo->expects($this->exactly(2))
            ->method('where')
            ->withConsecutive(['name', '=', 'Foo'], ['bar_id', '=', 1]);
        $fooRepo->expects($this->once())->method('get');

        $result = $fooEntity->where('name', '=', 'Foo')->where('bar_id', '=', 1)->get();
        $this->assertEquals(1, $result->count());
        $this->assertEquals('Foo', $result->first()->name);
    }

    /** @test */
    public function it_can_retrieve_a_relation_with_pivot_data()
    {
        list($fooRepo, $fooEntity) = $this->mockEntityWithRepo('Foo');
        $this->mockEntityWithRepo('Bar');

        $fooRepo->method('getArrayFromQueryResult')->willReturn([
            $this->getModelArray('FooModel', [], ['bars' => [
                $this->getModelArray('BarModel', [], [], ['name' => 'FooBar']),
            ]]),
        ]);

        $foo = $fooEntity->with('bars')->get()->first();
        $this->assertEquals('FooBar', $foo->bars->first()->getPivotData('name'));
        $this->assertEquals(collect(['name' => 'FooBar']), $foo->bars->first()->getPivotData());
    }

    /** @test */
    public function it_can_create_an_entity()
    {
        $repo = $this->createMock(QueryBuilderTestRepoStub::class);
        $entity = $this->createMock(QueryBuilderTestEntityStub::class);
        $query = $this->getMockFor(QueryBuilderTestQueryStub::class, ['buildEntity' => $entity], true, [$repo, $entity]);
        $entity = $this->getMockFor(QueryBuilderTestEntityStub::class, ['newQuery' => $query]);
        $repo->method('create')->willReturn([]);

        $repo->expects($this->once())->method('create')->with(['name' => 'Foo']);
        $entity->create(['name' => 'Foo']);
    }

    /** @test */
    public function it_can_update_an_entity()
    {
        $repo = $this->createMock(QueryBuilderTestRepoStub::class);
        $entity = $this->createMock(QueryBuilderTestEntityStub::class);
        $query = $this->getMockFor(QueryBuilderTestQueryStub::class, ['buildEntity' => $entity], true, [$repo, $entity]);
        $entity = $this->getMockFor(QueryBuilderTestEntityStub::class, ['newQuery' => $query]);
        $repo->expects($this->once())->method('update')->with(['name', '=', 'Foo'], null, null);
        $entity->where('name', '=', 'Foo')->update(['name', '=', 'Foo']);
    }

    /** @test */
    public function it_can_delete_an_entity()
    {
        $repo = $this->createMock(QueryBuilderTestRepoStub::class);
        $entity = $this->createMock(QueryBuilderTestEntityStub::class);
        $query = $this->getMockFor(QueryBuilderTestQueryStub::class, ['buildEntity' => $entity], true, [$repo, $entity]);
        $entity = $this->getMockFor(QueryBuilderTestEntityStub::class, ['newQuery' => $query]);
        $repo->expects($this->once())->method('delete');
        $entity->where('name', '=', 'Foo')->delete();
    }

    protected function mockEntityWithRepo($name)
    {
        $fooRepo = $this->createMock(BaseRepositoryContract::class);
        $fooRepo->method('newInstance')->will($this->returnSelf());
        $fooEntity = $this->getMockForAbstractClass(BaseEntity::class, [$fooRepo], $name . 'Entity');
        $this->app->when($name . 'Entity')
            ->needs(BaseRepositoryContract::class)
            ->give(function () use ($fooRepo) {
                return $fooRepo;
            });
        EntityHelpers::modelMap([
            $name . 'Model' => $name . 'Entity',
        ]);
        return [$fooRepo, $fooEntity];
    }

    protected function getModelArray($modelName = 'FooModel', $attributes = [], $relations = [], $pivotData = [])
    {
        return [
            'model' => $modelName,
            'attributes' => $attributes,
            'relations' => $relations,
            'pivot' => $pivotData
        ];
    }
}

class QueryBuilderTestQueryStub extends QueryBuilder {}
class QueryBuilderTestEntityStub extends BaseEntity {}
class QueryBuilderTestRepoStub extends BaseRepository {}
