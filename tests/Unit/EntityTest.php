<?php

namespace Tests\Feature;

use App\Entities\BaseEntity;
use App\Entities\ThreadEntity;
use App\Repositories\Base\BaseRepositoryContract;
use App\Repositories\Base\MySQL\BaseRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EntityTest extends TestCase
{
    protected $testEntityClass;

//    public function setUp()
//    {
//        parent::setUp();
//        $this->createTestTables('test_models');
//        $this->testEntityClass = app()->make(TestEntity::class);
//    }


    /** @test */
    public function it_can_update_an_entity()
    {
        $repo = $this->getMockFor(EntityTestRepoStub::class, ['update' => 'update called!']);
        $entity = $this->getMockFor(EntityTestEntityStub::class,
            ['buildEntity' => new EntityTestEntityStub($repo), 'getId' => 1], true, [$repo]);
        $attributes = ['name' => 'Foo'];

        $repo->expects($this->once())->method('update')->with($attributes, 1, 'some-key');

        $this->assertEquals(true, $entity->update($attributes));
    }

    /** @test */
    public function it_can_delete_an_entity()
    {
        $repo = $this->getMockFor(EntityTestRepoStub::class, ['delete' => true]);
        $entity = $this->getMockFor(EntityTestEntityStub::class,
            ['buildEntity' => new EntityTestEntityStub($repo), 'getId' => 1], true, [$repo]);
        $attributes = ['name' => 'Foo'];

        $repo->expects($this->once())->method('delete')->with(1, 'some-key');

        $this->assertEquals(true, $entity->delete());
    }

//    /** @test */
//    function cannot_create_a_new_entity_from_a_returned_entity()
//    {
//        $entity = $this->testEntityClass->create([]);
//        $this->assertNotNull($this->testEntityClass->getLast());
//
//        $this->expectException(RestrictedEntity::class);
//        $this->expectExceptionMessage('Action not permitted. Please use a service to perform this action.');
//
//        $entity->create([]);
//    }
//
//    /** @test */
//    function cannot_update_a_new_entity_from_a_returned_entity()
//    {
//        $entity = $this->testEntityClass->create([]);
//        $this->assertNotNull($this->testEntityClass->getLast());
//
//        $this->expectException(RestrictedEntity::class);
//        $this->expectExceptionMessage('Action not permitted. Please use a service to perform this action.');
//
//        $entity->update([]);
//    }
//
//    /** @test */
//    function cannot_delete_a_new_entity_from_a_returned_entity()
//    {
//        $entity = $this->testEntityClass->create([]);
//        $this->assertNotNull($this->testEntityClass->getLast());
//
//        $this->expectException(RestrictedEntity::class);
//        $this->expectExceptionMessage('Action not permitted. Please use a service to perform this action.');
//
//        $entity->delete([]);
//    }
}

class EntityTestEntityStub extends BaseEntity
{
    protected $primaryKey = 'some-key';

    protected $guarded = [];
}

class EntityTestRepoStub extends BaseRepository
{
}